@echo off
cls
call npm run clean
@echo building code .......
call npm run build
IF [%1]==[] ( goto :Deploy) ELSE ( goto :run)

:Deploy
    echo Build completed and coping files
    cd dist\expenseManagement
    call xcopy *.* I:\FOXTEAM\foxteam_web\InfoxTechApp\StaticApps\AngularExpenseApp /E /Y
    call xcopy *.* F:\FoxTeam\Releases\CCP\StaticApps\AngularExpenseApp /E /Y
    cd ..
    cd ..
goto :eof

:run
     echo Build completed and running code
     cd dist
     call start http://localhost:5000
    serve -s build
goto :eof
