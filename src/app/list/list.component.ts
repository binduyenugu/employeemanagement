import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ApiService, ExpenseStatus } from '.././api.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MessageService, PrimeNGConfig } from 'primeng/api';

declare const foxTeam: any;


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [DatePipe,MessageService] 
})
export class ListComponent implements OnInit, OnChanges {


  userId?: any;
  personalExpenseList: any = [];
  currentDate: Date;
  allsubProjects?: any;
  projectList?: any;
  allsubProjectList = [];
  CompanyId?: any;
  typeofExpenses?: any;
  expenseDetails: any;
  employeeNameDetails: any;
  Username: string;

  loaded = {
    Draft: false,
    Pending: false,
    Approved: false,
    PartiallyApproved: false,
    Rejected: false,
    paid: false
  };

  

  constructor(private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private api: ApiService, private router: Router, private datepipe: DatePipe) {
    this.CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
    this.userId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
    this.getAllExpensesDraft();

  }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  getAllExpensesDraft = () => {
    if (!this.loaded.Draft) {
      this.api.getAllPersonalExpenses(ExpenseStatus.Draft).subscribe(
        (res) => {

          this.FilterOut();
          this.personalExpenseList = this.personalExpenseList.concat(res);
          this.loaded.Draft = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  };

  handleChange(e) {
    var index = e.index;
    console.log(index);
    switch (index) {
      case 0:
        this.getAllExpensesDraft();
        console.log(ExpenseStatus.Draft);
        break;
      case 1:
        this.getAllExpensesPending();
        console.log(ExpenseStatus.Submmited);
        break;

      case 2:
        this.getAllExpensesApproved();
        break;

      case 3:
        this.getAllExpensesPartiallyApproved();
        break;

      case 4:
        this.getAllExpensesRejected();
        break;

      case 5:
        this.getAllExpensesPaid();
        break;

    }
  }
  getAllExpensesRejected() {
    
    console.log("getAllExpensesRejected")
    if (!this.loaded.Rejected) {
      this.api.getAllPersonalExpenses(ExpenseStatus.Rejected).subscribe(
        (res) => {
          this.FilterOut(ExpenseStatus.Rejected);

          this.personalExpenseList = this.personalExpenseList.concat(res);
          this.loaded.Rejected = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  }

  getAllExpensesPaid() {
    console.log("getAllExpensesPaid")
    if (!this.loaded.paid) {
      this.api.getAllPersonalExpenses(ExpenseStatus.Paid).subscribe(
        (res) => {
          this.FilterOut(ExpenseStatus.Paid);

          this.personalExpenseList = this.personalExpenseList.concat(res);
          this.loaded.paid = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  }

  getAllExpensesPending = () => {
    if (!this.loaded.Pending) {
      this.api.getAllPersonalExpenses(ExpenseStatus.Submmited).subscribe(
        (res) => {
          this.FilterOut(ExpenseStatus.Submmited);

          this.personalExpenseList = this.personalExpenseList.concat(res);
          this.loaded.Pending = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  };

  getAllExpensesApproved = () => {
    if (!this.loaded.Approved) {
      this.api.getAllPersonalExpenses(ExpenseStatus.Approved).subscribe(
        (res) => {
          this.FilterOut(ExpenseStatus.Approved);

          this.personalExpenseList = this.personalExpenseList.concat(res);
          this.loaded.Approved = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  };


  getAllExpensesPartiallyApproved = () => {
    if (!this.loaded.PartiallyApproved) {
      this.api.getAllPersonalExpenses(ExpenseStatus.PartiallyApproved).subscribe(
        (res) => {
          this.FilterOut(ExpenseStatus.Approved);

          this.personalExpenseList = this.personalExpenseList.concat(res);

          this.loaded.PartiallyApproved = res.length > 0;
        },
        (err) => {
          console.log(JSON.stringify(err));
        }
      );
    }
  };


  getDiferenceInDays(submitdate): number {
    return Math.abs(new Date(submitdate).getTime() - new Date().getTime()) / (1000 * 60 * 60 * 24);
  }

  getDate(expenseDate) {
    return this.datepipe.transform(expenseDate, 'MM/dd/yyyy');
  }

  getExpenseType(expenseId) {
    for (let i in this.typeofExpenses) {
      if (this.typeofExpenses[i]['id'] === expenseId) {
        return this.typeofExpenses[i]['Title'];
      }
    }
  }

  getProjectTitle(projectId) {
    for (let i in this.projectList) {
      if (this.projectList[i]['ProjectCode'] === projectId) {
        return this.projectList[i]['ProjectTitle'];
      }
    }
  }

  getSubProjectTitle(subProjectId) {
    for (let i in this.allsubProjectList) {
      if (this.allsubProjectList[i]['SubProjectID'] === subProjectId) {
        return this.allsubProjectList[i]['SubProjectTitle'];
      }
    }
  }

  editExpense(event) {
    console.log('Event Id ' + event);
    this.router.navigate(['/'], { queryParams: { 'editId': event } });
  }

  FilterOut(status  = ExpenseStatus.Draft){
    this.personalExpenseList = this.personalExpenseList.filter((x) => x.Status  != status);
  }

  resubmitExpense(event) {
    debugger;
    this.currentDate = new Date();
    this.api.getOneExpense(event).subscribe(res => {
      this.expenseDetails = res;
    },
      (err) => {
        console.log(err);
      },
      () => {
        this.messageService.add({severity:'info', summary: 'resubmit', detail: 'Resubmitting....'});
   
        var updatedExpenses = {
          ExpenseId: this.expenseDetails['ExpenseId'],
          EmployeeId: this.expenseDetails['EmployeeId'],
          CompanyId: this.expenseDetails['CompanyId'],
          ExpenseTypeId: this.expenseDetails['ExpenseTypeId'],
          ProjectCode: this.expenseDetails['ProjectCode'],
          SubProjectId: this.expenseDetails['SubProjectId'],
          ExpenseDate: this.expenseDetails['ExpenseDate'],
          SubmittedDate: this.currentDate,
          Location: this.expenseDetails['Location'],
          AmountRequested: this.expenseDetails['AmountRequested'],
          DocumentsSubmitted: '',
          AmountApproved: this.expenseDetails['AmountApproved'],
          Status: ExpenseStatus.Submmited,
          ApporvarUserId: this.expenseDetails['ApporvarUserId'],
          ApprovedOn: this.expenseDetails['ApprovedOn'],
          IsApproved: this.expenseDetails['IsApproved'],
          ApprovalRemarks: this.expenseDetails['ApprovalRemarks'],
          IsPaid: this.expenseDetails['IsPaid'],
          PaymentRemarks: this.expenseDetails['PaymentRemarks'],
          PaymentDate: this.expenseDetails['PaymentDate']
        };
 
        this.api.updateExpense(event, updatedExpenses).subscribe(
          (res) => {
            this.router.navigate(['view'])
              .then(() => {
                debugger;
                this.loaded.Draft = false;
                this.loaded.Pending = false;
                this.loaded.Rejected =false;
                this.getAllExpensesRejected();
                this.getAllExpensesDraft();
                this.getAllExpensesPending();
                this.messageService.add({severity:'success', summary: 'Success', detail: 'Expense resubmitted...'});
              });
          },
          (err) => {
            console.log('Error');
          }
        );
      }
    );
  }

  ngOnInit(): void {
    var FoxTeam = new foxTeam;
    FoxTeam.Ready(async () => {
      FoxTeam.RefreshValues();
      FoxTeam.WhoAmI();
      this.projectList = await FoxTeam.GetProjects(false);
      for (var i = 0; i < this.projectList.length; i++) {
        this.allsubProjects = await FoxTeam.GetSubProjects(this.projectList[i]['ProjectCode'], false);
        this.allsubProjectList = Array.from(new Set(this.allsubProjectList.concat(this.allsubProjects)));
        this.employeeNameDetails = await FoxTeam.GetEmployeeInfo(this.userId, false);
        this.Username = this.employeeNameDetails['UserName'];
      }

    });

    this.api.getExpenseTypes(this.CompanyId).subscribe(
      (res) => {
        console.log(res);
        this.typeofExpenses = res;
      },
      (err) => {
        console.log(err);
      });
  }

}
