import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { OutputCaptureComponent } from '../output-capture/output-capture.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, ExpenseStatus } from '.././api.service';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { FileUploadModule } from 'primeng/fileupload';

import { Dropdown } from "primeng/dropdown";

declare const foxTeam: any;


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css'],
  providers: [DatePipe, MessageService, FileUploadModule]
})
export class FormsComponent implements OnInit, AfterViewInit {
  @ViewChild('f') expenseForm: NgForm;
  @ViewChild('exptype') private drpExpType: Dropdown;
  expenseType?: any;
  allProjectList?: any;
  typeofExpenses?: any;
  projectList?: any;
  allsubProjects?: any;
  subProjectList = [];
  employeeInfo?: any;
  isActive?: boolean = false;
  BranchId?: String;
  CompanyId?: String;
  BaseUrl?: String;
  EmployeeId?: any;
  editExpenseDetails?: any;
  edit_id: any;
  expenseStatus: String;
  subProjectTitle: String;
  expenseTitle: String;
  projectTitle: String;
  contents: any = null;
  filename: string;
  isEditing: boolean = false;
  uploadUrl:string  =  "";

  @ViewChild(OutputCaptureComponent, { static: true }) outputCapture: OutputCaptureComponent;

  constructor(private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private api: ApiService,
    private _route: ActivatedRoute,
    private datepipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute) {

    this.CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
    this.BaseUrl = (<HTMLInputElement>document.getElementById('BaseUrl')).value;
    this.BranchId = (<HTMLInputElement>document.getElementById('BranchId')).value;
    this.EmployeeId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
  }



  ngOnInit(): void {
    this.primengConfig.ripple = true;
    var FoxTeam = new foxTeam;
    FoxTeam.Ready(async () => {
      FoxTeam.RefreshValues();
      FoxTeam.WhoAmI();
      this.employeeInfo = await FoxTeam.GetEmployeeInfo(this.EmployeeId, false);
      this.projectList = await FoxTeam.GetProjects(false);
      this.api.getExpenseTypes(+this.CompanyId).subscribe(
        (res) => {
          this.typeofExpenses = res;
        },
        (err) => {
          console.log(err);
        },
        () => {
          //console.log("employeeInfo Info     "+JSON.stringify(this.employeeInfo));
          this._route.queryParamMap.subscribe(parameterMap => {
            this.edit_id = parameterMap.get('editId');
            if (this.edit_id) {
              this.getExpenseDetails(this.edit_id);
            } else {
              this.isActive = false;
              this.expenseType = this.typeofExpenses;
              this.allProjectList = this.projectList;
              this.expenseForm.reset({});
            }
          });
        }
      );
    });
  }

  ngAfterViewInit() {

     }
  subProjects() {
    var FoxTeam = new foxTeam;
    if (this.expenseForm.value.selectAssignment) {
      let code = this.expenseForm.value.selectAssignment['ProjectCode'];
      if (code) {
        FoxTeam.Ready(async () => {
          FoxTeam.RefreshValues();
          FoxTeam.WhoAmI();
          this.subProjectList = await FoxTeam.GetSubProjects(code, false);
          if (this.subProjectList.length) {
            this.isActive = true;
          }
          else {
            this.isActive = false;
          }
        });
      }
    }
  }
  onDocUploaded(event){
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Document uploaded' });

    this.editExpenseDetails = event.originalEvent.body;
  }

  getPatch(url:string){
    if(url === null) return '';
    return url + '?cache=' + Math.random().toString();
  }

  deleteImage(type:string){
    let deleteUrl = `${this.BaseUrl}/Expense/deleteDoc?CompanyId=${this.CompanyId}&expenseid=${this.edit_id}&BlobId=${type}`;
    this.api.deleteProof(deleteUrl).subscribe((res) => {
      this.editExpenseDetails = {...res};
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Document Removed' });

    })
  }


  getExpenseDetails(id) {

    this.uploadUrl = `${this.BaseUrl}/Expense/uploaddoc?CompanyId=${this.CompanyId}&EmployeeId=${this.EmployeeId}&expenseid=${id}`;

    this.api.getOneExpense(id).subscribe(res => {
      this.editExpenseDetails = res;
      this.isEditing = true;
      console.log(this.editExpenseDetails);
    },
      (err) => {
        console.log(err);
      },
      () => {
        for (var i = 0; i < this.projectList.length; i++) {
          if (this.projectList[i]['ProjectCode'] === this.editExpenseDetails['ProjectCode']) {
            this.projectTitle = this.projectList[i]['ProjectTitle'];
          }
        }
        for (var j = 0; j < this.typeofExpenses.length; j++) {
          if (this.typeofExpenses[j]['id'] === this.editExpenseDetails['ExpenseTypeId']) {
            this.expenseTitle = this.typeofExpenses[j]['Title'];
          }
        }
        if (this.editExpenseDetails['SubProjectId']) {
          this.isActive = true;
          var FoxTeam = new foxTeam;
          FoxTeam.Ready(async () => {
            FoxTeam.RefreshValues();
            FoxTeam.WhoAmI();
            this.allsubProjects = await FoxTeam.GetSubProjects(this.editExpenseDetails['ProjectCode'], false);
            for (var k = 0; k < this.allsubProjects.length; k++) {
              if (this.allsubProjects[k]['SubProjectID'] === this.editExpenseDetails['SubProjectId']) {
                this.subProjectTitle = this.allsubProjects[k]['SubProjectTitle'];
                this.setExpenseValues();
              }
            }
          });
        }
        else {
          this.subProjectTitle = null;
          this.setExpenseValues();
        }
      });


  }

  setExpenseValues() {
    console.log(this.editExpenseDetails, "setExpenseValues")
    this.expenseForm.setValue({
      selectExpenseType: { 'Title': this.expenseTitle },
      selectAssignment: { 'ProjectTitle': this.projectTitle },
      selectSubProject: { 'SubProjectTitle': this.subProjectTitle },
      expenseDate: this.datepipe.transform(this.editExpenseDetails['ExpenseDate'], 'MM/dd/yyyy'),
      expensesValue: this.editExpenseDetails['AmountRequested'],
      expLocation: this.editExpenseDetails['Location'],
      ExpRemarks: this.editExpenseDetails['ExpRemarks']

    });
    console.log("ProjectTitle: " + this.projectTitle + "  Title:  " + this.expenseTitle + "  SubProjectTitle: " + this.subProjectTitle);
    this.expenseType = [this.expenseForm.value.selectExpenseType];
    this.allProjectList = [this.expenseForm.value.selectAssignment];
    this.subProjectList = [this.expenseForm.value.selectSubProject];
  }

  draftExpenses() {
    this.expenseStatus = 'draft';
    this.onSubmit();
  }

  onSubmit() {

    const ExpRemarks = this.expenseForm.value.ExpRemarks;
    const expenseDate = this.expenseForm.value.expenseDate;
    const currentDate = new Date();
    let status: ExpenseStatus;
    let subProjectID;
    if (!(this.expenseStatus)) {
      status = ExpenseStatus.Submmited;
    }
    else {
      status = ExpenseStatus.Draft;
    }
    if (this.expenseForm.value.selectSubProject != null) {
      subProjectID = this.expenseForm.value.selectSubProject['SubProjectID'];
    } else {
      subProjectID = 0;
    }

    if (this.edit_id) {
      const updatedExpenses = {
        ExpenseId: this.edit_id,
        EmployeeId: this.EmployeeId,
        CompanyId: this.CompanyId,
        ExpenseTypeId: this.editExpenseDetails['ExpenseTypeId'],
        ProjectCode: this.editExpenseDetails['ProjectCode'],
        SubProjectId: this.editExpenseDetails['SubProjectId'],
        ExpenseDate: expenseDate,
        SubmittedDate: currentDate,
        Location: this.expenseForm.value.expLocation,
        AmountRequested: this.expenseForm.value.expensesValue,
        docUrl  : this.editExpenseDetails.docUrl,
        docUrl2  : this.editExpenseDetails.docUrl2,
        docUrl3  : this.editExpenseDetails.docUrl3,
        ExpRemarks :  ExpRemarks,
        AmountApproved: 0,
        DocumentsSubmitted: '',
        Status: status,
        ApporvarUserId: '',
        ApprovedOn: null,
        IsApproved: false,
        IsPaid: false,
        PaymentRemarks: '',
        PaymentDate: null,
        ApprovalRemarks: ''

      }
      this.api.updateExpense(this.edit_id, updatedExpenses).subscribe(
        (res) => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Expense Updated' });
          setTimeout(() => {
            this.router.navigate(['MyExpense'])
          }, 1000)
        },
        (err) => {
          console.log("Error");
        }
      );
    }
    else {

      const addExpenseValues = {
        EmployeeId: this.EmployeeId,
        CompanyId: this.CompanyId,
        ExpenseTypeId: this.expenseForm.value.selectExpenseType['id'],
        ProjectCode: this.expenseForm.value.selectAssignment['ProjectCode'],
        SubProjectId: subProjectID,
        ExpenseDate: expenseDate,
        SubmittedDate: currentDate,
        Location: this.expenseForm.value.expLocation,
        AmountRequested: this.expenseForm.value.expensesValue,
        ExpRemarks:this.expenseForm.value.ExpRemarks,
        AmountApproved: 0,
        DocumentsSubmitted: '',
        Status: status,
        ApporvarUserId: '',
        ApprovedOn: null,
        IsApproved: false,
        IsPaid: false,
        PaymentRemarks: '',
        PaymentDate: null,
        ApprovalRemarks: ''
      }

      this.api.postExpense(addExpenseValues).subscribe(
        (res) => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Expenses added.' });
          setTimeout(() => {
            this.router.navigate(['MyExpense']);
          }, 500);
        },
        (err) => {
          console.log("Error " + JSON.stringify(err));
        }
      );
    }

  }


}
