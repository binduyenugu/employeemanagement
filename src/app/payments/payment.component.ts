import { Component, OnInit } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { ApiService, ExpenseStatus } from '../api.service';
import { DatePipe } from '@angular/common';
import { Append } from '../Common';

declare const foxTeam: any;


@Component({
    selector: 'payments-component',
    templateUrl: './payments.component.html',
    styleUrls: ['./payment.component.css'],
    providers: [MessageService, DatePipe]
})

export class PaymentsComponent implements OnInit {


    Remarks: string = "";
    SearchDate: Date = new Date();
    DateString = () => this.datePipe.transform(this.SearchDate, "MM/dd/yyyy")
    Data: any = [];
    CompanyId: string;
    BaseUrl: string;
    BranchId: string;
    EmployeeId: string;
    display: boolean = false;
    currntItem: any = null;
    currentDisplayImage: any;
    displayImageDialog: boolean;
    constructor(private messageService: MessageService,
        private primengConfig: PrimeNGConfig,
        private api: ApiService,
        private datePipe: DatePipe
    ) { }

    ngOnInit() {

        this.CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
        this.BaseUrl = (<HTMLInputElement>document.getElementById('BaseUrl')).value;
        this.BranchId = (<HTMLInputElement>document.getElementById('BranchId')).value;
        this.EmployeeId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
        this.RefreshData();
    }




    setRecord(item) {
        console.log(item);
        this.currntItem = item;
        this.display = true;
    }

    Pay() {
        this.currntItem.isPaid = true;
        this.currntItem.PaymentRemarks = Append(this.currntItem.PaymentRemarks ,this.Remarks );
        this.currntItem.PaymentDate = new Date();
        this.currntItem.Status = ExpenseStatus.Paid;
        if((this.currntItem.PaymentRemarks ?? "") == "" || (this.currntItem.PaymentCycle ?? "") == ""){
            this.messageService.add({ severity: 'error', summary: 'error', detail: 'Payment cycle and remarks required.' });   
            return;
        }
        this.api.updateExpense(this.currntItem.ExpenseId, this.currntItem).subscribe((d) => {
            this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Amount Paid' });
            this.Data = this.Data.filter((x) => x.ExpenseId != d.ExpenseId)
            this.display = false;
        },(err) => {
            this.messageService.add({ severity: 'error', summary: 'error', detail: 'Error while payment' });   
        })
    }
    Cancel() {
        this.display = false;
    }

    Reject() {
        this.currntItem.IsApproved = false;
        this.currntItem.ApprovalRemarks =  Append(this.currntItem.ApprovalRemarks , this.Remarks);
        
        this.currntItem.ApprovedOn = new Date();
        this.currntItem.ApporvarUserId = this.EmployeeId;
        this.currntItem.Status = ExpenseStatus.Rejected;
        this.api.updateExpense(this.currntItem.ExpenseId, this.currntItem).subscribe((d) => {
            this.messageService.add({ severity: 'warn', summary: 'Warn', detail: 'Amount Rejected' });
            this.Data = this.Data.filter((x) => x.ExpenseId != d.ExpenseId)
            this.display = false;
        },(err) => {
            this.messageService.add({ severity: 'error', summary: 'error', detail: 'Error while rejecting' });
            
        })
    }

    viewFullImage(img) {
        this.currentDisplayImage = img;
        this.displayImageDialog = true;
    }
    RefreshData() {


        this.api.getAllUnpaidExpenses(+this.CompanyId, this.DateString()).subscribe((data) => {
            this.Data = data;

            console.log(this.Data);
        })
    }
}