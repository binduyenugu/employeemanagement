import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExpensetypeMaster } from './Common';

export enum ExpenseStatus
{
  Draft,
  Submmited,
  Rejected,
  Approved,
  PartiallyApproved,
  Paid
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  readonly BaseUrl = (<HTMLInputElement>document.getElementById('BaseUrl')).value;
  readonly CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
  readonly EmployeeId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
 

  private headers:HttpHeaders;
  private apiURL:string = this.BaseUrl+'Expense';
  private apiExpenseType:string = this.BaseUrl+'expensetype';

  constructor(private http: HttpClient) { 
    this.headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  }
  
  deleteProof(url):Observable<any>{
    return this.http.get(url, {headers: this.headers})

  }
  getProjectList(): Observable<any>{
    const url = this.BaseUrl+'\\Timesheet\\GetProjectList?CompanyId='+this.CompanyId;
    return this.http.get(url, {headers: this.headers})

  }

  getSubProjectList(projectId: string): Observable<any>{
    const url = this.BaseUrl+'\\Timesheet\\GetSubProjectList?CompanyId='+this.CompanyId+'&ProjectId='+projectId;
    return this.http.get(url, {headers: this.headers})
  }

  getEmploy(empId: String): Observable<any>{
    const url = this.BaseUrl+'\\Employee\\oneEmployeeData?CompanyId='+this.CompanyId+'&empId='+empId;
    return this.http.get(url, {headers: this.headers})
  }

   /**
    * Get all expenses using CompanyId, EmployeeId, Status    */
  //   getExpenseTypes(companyId: String): Observable<any>{
  //     return this.http.get(`${this.BaseUrl}/ExpenseType?CompanyId=${this.CompanyId}`, {headers: this.headers});
  //  }

  
  getAllUnpaidExpenses(companyId: number , fromDate: any){
    return this.http.get(`${this.apiURL}/GetPendingPayments?companyId=${companyId.toString()}&fromDate=${fromDate}`)
  }

   /**
    * Get all expenses using CompanyId, EmployeeId, Status    */
    getAllPersonalExpenses(Status: ExpenseStatus): Observable<any>{
      return this.http.get(`${this.apiURL}?CompanyId=${this.CompanyId}&EmployeeId=${this.EmployeeId}&Status=${Status}`, {headers: this.headers});
   }

   /**
    * Get one expense data using expense id    */
   getOneExpense(id: number): Observable<any>{
     return this.http.get(`${this.apiURL}/${id}`, {headers: this.headers});
   }
    /**
    * Get all expenses data using expense id    */
     getAllExpenses(Status: ExpenseStatus): Observable<any>{
      return this.http.get(`${this.apiURL}/GetExpenseAdmin?CompanyId=${this.CompanyId}&AdminId=${this.EmployeeId}&Status=${Status}`, {headers: this.headers});
    }

   /**
    * post expense data into db    */
   postExpense(expenseData:any): Observable<any>{
     return this.http.post(`${this.apiURL}`, expenseData, {headers: this.headers});
   }

   /**
    * update expense using expense id    */
   updateExpense(id: number, expenseData:any): Observable<any>{
     return this.http.put(`${this.apiURL}/${id}`, expenseData, {headers: this.headers});
   }


   //#region  
      /**
    * Get all expenses using CompanyId, EmployeeId, Status    */
   getExpenseTypes(CompanyId:number , showFreezed:boolean = false):Observable<ExpensetypeMaster[]>
   {
    return this.http.get<ExpensetypeMaster[]>(`${this.apiExpenseType}?CompanyId=${this.CompanyId}&showFreezed=${showFreezed}`, {headers: this.headers});
   } 

   getExpenseType(Id:number):Observable<ExpensetypeMaster>
   {
    return this.http.get<ExpensetypeMaster>(`${this.apiExpenseType}/${this.CompanyId}`, {headers: this.headers});
   } 

   AddExpenseType(Expense):Observable<ExpensetypeMaster>
   {
    return this.http.post<ExpensetypeMaster>(`${this.apiExpenseType}`, Expense, {headers: this.headers});
   } 

   UpdateExpenseType(Expense):Observable<ExpensetypeMaster>
   {
    return this.http.put<ExpensetypeMaster>(`${this.apiExpenseType}/${Expense.id}`, Expense, {headers: this.headers});
   } 

   DeleteExpenseType(id:number): boolean
   {
    try {
      this.http.delete(`${this.apiExpenseType}/${id}`, {headers: this.headers});
       return true;
    } catch (error) {
      return false;
    }
   } 





   //#endregion

}
