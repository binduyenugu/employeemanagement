import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { FormsComponent } from './forms/forms.component';
import { ListComponent } from './list/list.component';
import { FormListComponent } from './form-list/form-list.component';
import {AuthGuardService} from './auth-guard.service';
import { PaymentsComponent } from './payments/payment.component';
import { ExpenseTypesComponent } from './expense-types/expense-types.component';

const appRoutes: Routes = [
    {path:'', component: FormsComponent},
    {path:'Admin',component: FormListComponent},
    {path:'MyExpense',component:ListComponent},
    {path:'payments',component:PaymentsComponent},
    {path:'ExpenseTypes',component:ExpenseTypesComponent}
  ];

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes,{enableTracing:true, onSameUrlNavigation: 'reload', useHash: true})
  
    ],
    exports:[RouterModule]
})

export class AppRoutingModule { }
