import { Component, OnInit } from '@angular/core';
import { ApiService } from '.././api.service';
import {MenuItem} from 'primeng/api';

declare const foxTeam:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  items: MenuItem[];
  empID?:any;
  financialEmployee?:Array<string> = [];
  isFinCtrl:boolean = false;

  constructor(private api:ApiService) {
    this.empID = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
    var FoxTeam = new foxTeam;
    FoxTeam.Ready(async () => {
        FoxTeam.RefreshValues();
        FoxTeam.WhoAmI();
        this.financialEmployee = await FoxTeam.GetEmployeeInfo(this.empID,false);   
        console.log(this.financialEmployee);   
      this.addItems();
      });
  
   
   }

   addItems = () => {
    if(this.financialEmployee['IsfinancialCtrl']===true){
      this.items = [
        {
          label:'Add Expenses',
          icon:'pi pi-fw pi-pencil',
          routerLink: '/'
          
        },
        {
          label:'My Expenses',
          icon:'pi pi-fw pi-wallet',
          routerLink: '/MyExpense'
        },          
        {
          label:'Admin Area',
          icon:'pi pi-fw pi-briefcase',
          routerLink: '/Admin'
        },
        {
          label:'Expense Types',
          icon:'pi pi-fw pi-book',
          routerLink: '/ExpenseTypes'
        },
        {
          label:'Payments',
          icon:'pi pi-fw pi-money-bill',
          routerLink: '/payments'
        }
      ];
    } else {
      this.items = [
        {
          label:'Add Expenses',
          icon:'pi pi-fw pi-pencil',
          routerLink: '/'
          
        },
        {
          label:'Personal Expenses',
          icon:'pi pi-fw pi-book',
          routerLink: '/MyExpense'
        }
      ];
    }
    
  }

  ngOnInit() {
  }
}
