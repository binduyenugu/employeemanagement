import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { ApiService } from '../api.service';
import { ExpensetypeMaster } from '../Common';

@Component({
  selector: 'expense-types',
  templateUrl: './expense-types.component.html',
  styleUrls: ['./expense-types.component.css'],
  providers : [MessageService]
})
export class ExpenseTypesComponent implements OnInit {
  addExpenseTitle:string;

  MasterExpenseTypes:ExpensetypeMaster[] = [];

  CompanyExpenseTypes:ExpensetypeMaster[] = [];

  CurrentExpense:ExpensetypeMaster = null;

  display = false;

  CompanyId: string;
  BaseUrl: string;
  BranchId: string;
  EmployeeId: string;
  expenseEdit:boolean=false;
  expenseTitle: string;

  constructor(private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private api: ApiService,
    private _route: ActivatedRoute,
    private router: Router,
    private route: ActivatedRoute) {

      this.CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
      this.BaseUrl = (<HTMLInputElement>document.getElementById('BaseUrl')).value;
      this.BranchId = (<HTMLInputElement>document.getElementById('BranchId')).value;
      this.EmployeeId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;

     }

  ngOnInit(): void {
    this.api.getExpenseTypes(+this.CompanyId , true).subscribe(res => {
      this.MasterExpenseTypes =  res.filter(x => x.CompanyId === 0);
      this.CompanyExpenseTypes =  res.filter(x => x.CompanyId !== 0);
    })
  }

  EditExpenseType(exptypes){
    this.CurrentExpense = exptypes;
    this.display = true;
  }

  addExpense(){
    let exp = <ExpensetypeMaster> {};
    exp.CompanyId = +this.CompanyId;
    exp.id = 0;
    exp.isCapex =false ; exp.FreezeInd = false;
    this.display = true;
    this.CurrentExpense = exp;
  }

  Save(){
    console.log(this.CurrentExpense);
    if(this.CurrentExpense.id === 0){
      this.api.AddExpenseType(this.CurrentExpense).subscribe(e => {
        this.CompanyExpenseTypes.push(e);
        this.CurrentExpense = e;
        this.display = false;
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Expense Type added' });

      });
    }else{
      this.api.UpdateExpenseType(this.CurrentExpense).subscribe(e => {
        this.CurrentExpense = e;
  this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Expense Type Updated' });
        this.display = false;

      });
    }

  }

  Cancel(){
    this.display = false;
  }

}
