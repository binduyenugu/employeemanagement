import { Component, OnInit, OnChanges } from '@angular/core';
import { ApiService, ExpenseStatus } from '.././api.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

declare const foxTeam: any;


@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css'],
  providers: [ApiService, DatePipe]
})



export class FormListComponent implements OnInit {


  expenseList: any[];
  expenseDetails: any;
  cols: Array<string> = [];
  displayDialog: boolean = false;
  displayImageDialog: boolean = false;
  displayReason: boolean = false;
  isActive: boolean = false;
  isRejectActive: boolean = false;
  reason: String;
  validReason: String;
  allPendingExpenseList: any;
  allApprovedExpenseList: any;
  allPartiallyApprovedExpenseList: any;
  allRejectedExpenseList: any;
  allPaidExpenseList: any;
  employeeDetails?: Array<string> = [];
  amountApproved: Number;
  approvalRemarks: string = null;
  typeofExpenses: any;
  projectList: any;
  allsubProjects: any;
  allsubProjectList: Array<string> = [];
  CompanyId: any;
  userId: any;
  employeeNameDetails: any;
  Username: string;

  constructor(private api: ApiService, private router: Router, private datepipe: DatePipe) {
    this.CompanyId = (<HTMLInputElement>document.getElementById('CompanyId')).value;
    this.userId = (<HTMLInputElement>document.getElementById('EmployeeId')).value;
    this.getAllExpenses();

  }


  handleChange(e) {
    var index = e.index;
    console.log(index);
    switch (index) {
      case 0: //pending
        this.getAllExpenses(ExpenseStatus.Submmited);

        break;
      case 1: //Approved
        this.getAllExpenses(ExpenseStatus.Approved);

        break;
      case 2: //Partially Approved
        this.getAllExpenses(ExpenseStatus.PartiallyApproved);

        break;

      case 3: //Rejected
        this.getAllExpenses(ExpenseStatus.Rejected);

        break;

      case 4: //Paid 
        this.getAllExpenses(ExpenseStatus.Paid);

        break;


    }
  }
  getAllExpenses = (status = ExpenseStatus.Submmited) => {
    switch (status) {

      case ExpenseStatus.Approved:
        this.api.getAllExpenses(ExpenseStatus.Approved).subscribe(
          (res) => {
            this.allApprovedExpenseList = res;
          },
          (err) => {
            console.log(JSON.stringify(err));
          }
        );
        break;
      case ExpenseStatus.PartiallyApproved:
        this.api.getAllExpenses(ExpenseStatus.PartiallyApproved).subscribe(
          (res) => {
            this.allPartiallyApprovedExpenseList = res;
          },
          (err) => {
            console.log(JSON.stringify(err));
          }
        );
        break;
      case ExpenseStatus.Rejected:
        this.api.getAllExpenses(ExpenseStatus.Rejected).subscribe(
          (res) => {
            this.allRejectedExpenseList = res;
          },
          (err) => {
            console.log(JSON.stringify(err));
          }
        );
        break;
      case ExpenseStatus.Paid:
        this.api.getAllExpenses(ExpenseStatus.Paid).subscribe(
          (res) => {
            this.allPaidExpenseList = res;
          },
          (err) => {
            console.log(JSON.stringify(err));
          }
        );
      case ExpenseStatus.Submmited:
      default:

        this.api.getAllExpenses(ExpenseStatus.Submmited).subscribe(
          (res) => {
            this.allPendingExpenseList = res;
          },
          (err) => {
            console.log(JSON.stringify(err));
          }
        );
        break;

    }



  }

  getEmployeeName(EmployeeId) {
    var FoxTeam = new foxTeam;
    FoxTeam.Ready(async () => {
      this.employeeNameDetails = await FoxTeam.GetEmployeeInfo(EmployeeId, false);
      this.Username = this.employeeNameDetails['UserName'];
    });
    return this.Username;
  }
  getDate(expenseDate) {
    return this.datepipe.transform(expenseDate, 'MM/dd/yyyy');
  }
  getExpenseType(expenseId) {
    for (let i in this.typeofExpenses) {
      if (this.typeofExpenses[i]['id'] === expenseId) {
        return this.typeofExpenses[i]['Title'];
      }
    }
  }

  getProjectTitle(projectId) {
    for (let i in this.projectList) {
      if (this.projectList[i]['ProjectCode'] === projectId) {
        return this.projectList[i]['ProjectTitle'];
      }
    }
  }

  getSubProjectTitle(subProjectId) {
    for (let i in this.allsubProjectList) {
      if (this.allsubProjectList[i]['SubProjectID'] === subProjectId) {
        return this.allsubProjectList[i]['SubProjectTitle'];
      }
    }
  }



  ngOnInit(): void {

    var FoxTeam = new foxTeam;
    FoxTeam.Ready(async () => {
      FoxTeam.RefreshValues();
      FoxTeam.WhoAmI();
      this.projectList = await FoxTeam.GetProjects(false);
      for (var i = 0; i < this.projectList.length; i++) {
        this.allsubProjects = await FoxTeam.GetSubProjects(this.projectList[i]['ProjectCode'], false);
        this.allsubProjectList = Array.from(new Set(this.allsubProjectList.concat(this.allsubProjects)));
      }

    });
    this.api.getExpenseTypes(this.CompanyId).subscribe(
      (res) => {
        console.log(res);
        this.typeofExpenses = res;
      },
      (err) => {
        console.log(err);
      });

  }


  onSelectExpense(event) {
    //this.expenseDetails = {...event.data}; 
    this.displayDialog = true;
    this.api.getOneExpense(event).subscribe(res => {
      this.expenseDetails = res;
    },
      (err) => {
        console.log(err);
      },
      () => {
        this.amountApproved = this.expenseDetails['AmountRequested'];
        var FoxTeam = new foxTeam;

        FoxTeam.Ready(async () => {

          this.employeeDetails = await FoxTeam.GetEmployeeInfo(this.expenseDetails['EmployeeId'], false);
        });

      }

    );
  }

  currentDisplayImage: string = ""
  viewFullImage(img) {
    this.currentDisplayImage = img;
    this.displayImageDialog = true;
  }
  
  approveEmployeeExpense() {
    // this.displayDialog = false;

    var approvedDate = new Date();
    if (this.amountApproved < this.expenseDetails['AmountRequested']) {
      var approvalStatus = ExpenseStatus.PartiallyApproved;
    }
    else {

      var approvalStatus = ExpenseStatus.Approved;
    }
    var updatedExpenses = {
      ExpenseId: this.expenseDetails['ExpenseId'],
      EmployeeId: this.expenseDetails['EmployeeId'],
      CompanyId: this.expenseDetails['CompanyId'],
      ExpenseTypeId: this.expenseDetails['ExpenseTypeId'],
      ProjectCode: this.expenseDetails['ProjectCode'],
      SubProjectId: this.expenseDetails['SubProjectId'],
      ExpenseDate: this.expenseDetails['ExpenseDate'],
      SubmittedDate: this.expenseDetails['SubmittedDate'],
      Location: this.expenseDetails['Location'],
      AmountRequested: this.expenseDetails['AmountRequested'],
      DocumentsSubmitted: '',
      AmountApproved: this.amountApproved,
      Status: approvalStatus,
      ApporvarUserId: this.userId,
      ApprovedOn: approvedDate,
      IsApproved: true,
      ApprovalRemarks: this.approvalRemarks,
      IsPaid: false,
      PaymentRemarks: '',
      PaymentDate: null
    }
    this.api.updateExpense(this.expenseDetails['ExpenseId'], updatedExpenses).subscribe(
      (res) => {
        this.displayDialog = false;
        this.router.navigate(['list'])
          .then(() => {
            this.getAllExpenses();
          });
      },
      (err) => {
        console.log("Error " + JSON.stringify(err));
      }
    );


  }

  approvedExpense() {

    if (this.amountApproved < this.expenseDetails['AmountRequested']) {
      this.isActive = true;
    }
    else {
      this.isActive = false;
    }
  }

  rejectExpense() {
    this.isActive = true;
    var approvedDate = new Date();
    if (this.approvalRemarks) {
      var rejectedExpense = {
        ExpenseId: this.expenseDetails['ExpenseId'],
        EmployeeId: this.expenseDetails['EmployeeId'],
        CompanyId: this.expenseDetails['CompanyId'],
        ExpenseTypeId: this.expenseDetails['ExpenseTypeId'],
        ProjectCode: this.expenseDetails['ProjectCode'],
        SubProjectId: this.expenseDetails['SubProjectId'],
        ExpenseDate: this.expenseDetails['ExpenseDate'],
        SubmittedDate: this.expenseDetails['SubmittedDate'],
        Location: this.expenseDetails['Location'],
        AmountRequested: this.expenseDetails['AmountRequested'],
        DocumentsSubmitted: '',
        AmountApproved: 0,
        Status: ExpenseStatus.Rejected,
        ApporvarUserId: this.userId,
        ApprovedOn: approvedDate,
        IsApproved: false,
        ApprovalRemarks: this.approvalRemarks,
        IsPaid: false,
        PaymentRemarks: '',
        PaymentDate: null
      }
      this.api.updateExpense(this.expenseDetails['ExpenseId'], rejectedExpense).subscribe(
        (res) => {
          this.displayDialog = false;
          this.isActive = false;
          this.router.navigate(['list'])
            .then(() => {
              this.approvalRemarks = '';
              this.getAllExpenses();
            });
        },
        (err) => {
          console.log("Error " + JSON.stringify(err));
        }
      );
    }
  }

}

