function Append(main:any , appendStr:string) {
    if(main == null)
        main = appendStr;
    else
        main += appendStr;

    return main;

}



interface ExpensetypeMaster {
    id:        number;
    CompanyId: number;
    Title:     string;
    isCapex:   boolean;
    FreezeInd: boolean;
}


export {
    Append,
    ExpensetypeMaster
}